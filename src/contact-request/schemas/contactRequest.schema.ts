import * as mongoose from 'mongoose';

export const ContactRequestSchema = new mongoose.Schema({
    name: String,
    email: String,
    phone: String,
    autoValue: String,
    year: String,
    engine: String,
    typeEngine: String,
    sum: String,

});
