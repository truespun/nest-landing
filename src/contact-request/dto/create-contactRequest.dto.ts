export interface CreateContactRequestDto {
     name: string;
     email: string;
     phone: string;
     autoValue: string;
     year: string;
     engine: string;
     typeEngine: string;
     sum: string;
}
