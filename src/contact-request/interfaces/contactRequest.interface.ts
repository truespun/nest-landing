import { Document } from 'mongoose';

export interface ContactRequest extends Document {
    readonly name: string;
    readonly email: string;
    readonly phone: string;
    readonly autoValue: string;
    readonly year: string;
    readonly engine: string;
    readonly typeEngine: string;
    readonly sum: string;
}
