import { Module } from '@nestjs/common';
import { MongooseModule } from '@nestjs/mongoose';
import { ContactRequestController } from './contactRequest.controller';
import { ContactRequestService } from './contactRequest.service';
import { ContactRequestSchema } from './schemas/contactRequest.schema';

@Module({
    imports: [MongooseModule.forFeature([{ name: 'ContactRequest', schema: ContactRequestSchema }])],
    controllers: [ContactRequestController],
    providers: [ContactRequestService],

})
export class ContactRequestModule {}
