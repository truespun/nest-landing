import { Controller, Get, Res, HttpStatus, Post, Body, UseGuards, Param, NotFoundException, Put, Delete } from '@nestjs/common';
import { ContactRequestService } from './contactRequest.service';
import { CreateContactRequestDto } from './dto/create-contactRequest.dto';
import { AuthGuard } from '@nestjs/passport';
import { ValidateObjectId } from '../shared/pipes/validate-object-id.pipes';

@Controller('contact-requests')
export class ContactRequestController {

    constructor(private contactRequestService: ContactRequestService) { }

    @UseGuards(AuthGuard('jwt'))
    @Get()
    async getContactsRequests(@Res() res) {
        const requests = await this.contactRequestService.getContactsRequests();
        return res.status(HttpStatus.OK).json(requests);
    }

    @Post()
    async addRequest(@Res() res, @Body() createContactRequestDto: CreateContactRequestDto) {
        const newRequest = await this.contactRequestService.addContactRequest(createContactRequestDto);
        return res.status(HttpStatus.OK).json({
            message: 'Request has been submitted successfully!',
            request: newRequest,
        });
    }

    @UseGuards(AuthGuard('jwt'))
    @Get(':requestID')
    async getRequest(@Res() res, @Param('requestID', new ValidateObjectId()) requestID) {
        const request = await this.contactRequestService.getContactsRequestsById(requestID);
        if (!request) { throw new NotFoundException('Post does not exist!'); }
        return res.status(HttpStatus.OK).json(request);

    }
    @UseGuards(AuthGuard('jwt'))
    @Put(':requestID')
    async editRequest(
      @Res() res,
      @Param('requestID', new ValidateObjectId()) requestID,
      @Body() createContactRequestDto: CreateContactRequestDto,
    ) {
        const editedRequest = await this.contactRequestService.editRequest(requestID, createContactRequestDto);
        if (!editedRequest) { throw new NotFoundException('Request does not exist!'); }
        return res.status(HttpStatus.OK).json({
            data: {editedRequest},
        });
    }
    @UseGuards(AuthGuard('jwt'))
    @Delete('/:requestID')
    async deleteRequest(@Res() res, @Param('requestID', new ValidateObjectId()) requestID) {
        const deletedRequest = await this.contactRequestService.deleteRequest(requestID);
        if (!deletedRequest) { throw new NotFoundException('Request does not exist!'); }
        return res.status(HttpStatus.OK).json({
            message: 'Request has been deleted!',
            request: deletedRequest,
        });
    }
}
