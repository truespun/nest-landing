import {Model} from 'mongoose';
import {Injectable} from '@nestjs/common';
import {InjectModel} from '@nestjs/mongoose';
import {ContactRequest} from './interfaces/contactRequest.interface';
import {CreateContactRequestDto} from './dto/create-contactRequest.dto';

@Injectable()
export class ContactRequestService {
    constructor(@InjectModel('ContactRequest') private readonly contactRequestModel: Model<ContactRequest>  ) {}

    async addContactRequest(createContactRequestDto: CreateContactRequestDto): Promise<ContactRequest> {
        const newContactRequest = await new this.contactRequestModel(createContactRequestDto);
        return newContactRequest.save();
    }

    async getContactsRequests(): Promise<ContactRequest[]> {
        return await this.contactRequestModel.find().exec();
    }

    async getContactsRequestsById(requestID): Promise<ContactRequest> {
        return await this.contactRequestModel.findById(requestID).exec();
    }

    async editRequest(requestID: string, createContactRequestDto: CreateContactRequestDto): Promise<ContactRequest> {
        return await this.contactRequestModel
          .findByIdAndUpdate(requestID, createContactRequestDto, {new: true});
    }

    async deleteRequest(requestID): Promise<any> {
        return await this.contactRequestModel
          .findByIdAndRemove(requestID);
    }
}
