export interface CreateContactConfigDto {
    phone: string;
    email: string;
    address: string;
    facebook: string;
    instagram: string;
    lat: string;
    lng: string;
}
