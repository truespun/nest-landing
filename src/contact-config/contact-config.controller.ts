import {
  Controller,
  Get,
  Res,
  HttpStatus,
  NotFoundException,
  Post,
  Body,
  Put,
  UseGuards,
  Param,
  Delete
} from '@nestjs/common';
import { ContactConfigService} from './contact-config.service';
import { CreateContactConfigDto } from './dto/contact-config.dto';
import {AuthGuard} from '@nestjs/passport';
import { ValidateObjectId } from '../shared/pipes/validate-object-id.pipes';

@Controller('contact-config')
export class ContactConfigController {
  constructor(private contactConfigService: ContactConfigService) {
  }

  @Get()
  async getContactConfig(@Res() res) {
    const contactConfig = await this.contactConfigService.getContactConfig();
    return res.status(HttpStatus.OK).json(contactConfig);
  }

  @UseGuards(AuthGuard('jwt'))
  @Post()
  async addConfig(@Res() res, @Body() createContactConfigDto: CreateContactConfigDto) {
    const newConfig = await this.contactConfigService.addContactConfig(createContactConfigDto);
    return res.status(HttpStatus.OK).json( newConfig );
  }

  @UseGuards(AuthGuard('jwt'))
  @Put(':configID')
  async editConfig(
    @Res() res,
    @Param('configID', new ValidateObjectId()) configID,
    @Body() createContactConfigDto: CreateContactConfigDto,
  ) {
    const editedConfig = await this.contactConfigService.editConfig(configID, createContactConfigDto);
    if (!editedConfig) {
      throw new NotFoundException('Config does not exist!');
    }
    return res.status(HttpStatus.OK).json(editedConfig);
  }

  @UseGuards(AuthGuard('jwt'))
  @Get(':configID')
  async getConfig(@Res() res, @Param('configID', new ValidateObjectId()) configID) {
    const data = await this.contactConfigService.getContactsConfigById(configID);
    if (!data) {
      throw new NotFoundException('Config does not exist!');
    }
    return res.status(HttpStatus.OK).json(data );
  }

  @UseGuards(AuthGuard('jwt'))
  @Delete('/:configID')
  async deleteRequest(@Res() res, @Param('configID', new ValidateObjectId()) configID) {
    const deletedConfig = await this.contactConfigService.deleteConfig(configID);
    if (!deletedConfig) {
      throw new NotFoundException('Request does not exist!');
    }
    return res.status(HttpStatus.OK).json({
      data: { deletedConfig },
    });
  }

}
