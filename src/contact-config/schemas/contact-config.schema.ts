import * as mongoose from 'mongoose';

export const ContactConfigSchema = new mongoose.Schema({
    phone: String,
    email: String,
    address: String,
    facebook: String,
    instagram: String,
    lat: String,
    lng: String,
});
