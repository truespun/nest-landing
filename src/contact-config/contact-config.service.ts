import {Model} from 'mongoose';
import {Injectable} from '@nestjs/common';
import {InjectModel} from '@nestjs/mongoose';
import {ContactConfig} from './interfaces/contact-config.interface';
import {CreateContactConfigDto} from './dto/contact-config.dto';

@Injectable()
export class ContactConfigService {
    constructor(@InjectModel('ContactConfig') private readonly contactConfigModel: Model<ContactConfig>  ) {}

    async getContactConfig(): Promise<ContactConfig[]> {
        return await this.contactConfigModel.find().exec();
    }

    async addContactConfig(createContactConfigDto: CreateContactConfigDto): Promise< ContactConfig|string > {
        const existContact: ContactConfig[] = await this.getContactConfig();
        if (existContact.length < 1) {
            const newContactConfig = await new this.contactConfigModel(createContactConfigDto);
            return newContactConfig.save();
        } else {
            return 'Contact config  is already exist';
        }
    }

    async getContactsConfigById(configId): Promise<ContactConfig | object> {
      return await this.contactConfigModel.findById(configId).exec();
    }

    async editConfig(configID: string, createContactConfigDto: CreateContactConfigDto): Promise<ContactConfig | any> {
      return this.contactConfigModel.findByIdAndUpdate(configID, createContactConfigDto, { new: true });
    }

    async deleteConfig(configID): Promise<any> {
        return this.contactConfigModel.findByIdAndRemove(configID);
    }
}
