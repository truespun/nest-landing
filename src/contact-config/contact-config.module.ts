import { Module } from '@nestjs/common';
import { ContactConfigService } from './contact-config.service';
import { ContactConfigController } from './contact-config.controller';
import {MongooseModule} from '@nestjs/mongoose';
import {ContactConfigSchema} from './schemas/contact-config.schema';

@Module({
  imports: [MongooseModule.forFeature([{ name: 'ContactConfig', schema: ContactConfigSchema }])],
  providers: [ContactConfigService],
  controllers: [ContactConfigController],
})
export class ContactConfigModule {}
