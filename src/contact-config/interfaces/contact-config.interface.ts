import { Document } from 'mongoose';

export interface ContactConfig extends Document {
    readonly phone: string;
    readonly email: string;
    readonly address: string;
    readonly facebook: string;
    readonly instagram: string;
    readonly lat: string;
    readonly lng: string;
}
