import {Controller, Get, Res, HttpStatus, Body, UseGuards} from '@nestjs/common';
import { UsersService} from './users.service';
import { LoginUserDto } from './dto/login-user.dto';
import {AuthGuard} from '@nestjs/passport';

@Controller('users')
export class UsersController {
    constructor(private usersService: UsersService) { }

    @UseGuards(AuthGuard('jwt'))
    @Get()
    async findOne(@Res() res, @Body() body: LoginUserDto) {
        const user = await this.usersService.findOne( body.username );
        return res.status(HttpStatus.OK).json(user);
    }
}
