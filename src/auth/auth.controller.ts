 import {Body, Controller, Get, HttpStatus, Post, Request, Res, UseGuards, Delete, Param} from '@nestjs/common';
 import {AuthService} from './auth.service';
 import {AuthGuard} from '@nestjs/passport';
 import {CreateUserDto} from '../users/dto/create-user.dto';
 import {LoginUserDto} from '../users/dto/login-user.dto';

 @Controller('auth')
export class AuthController {
    constructor(private readonly  authService: AuthService) {}

    @Post('/register')
    async createUser(@Res() res, @Body() createUserDto: CreateUserDto) {
        const newUser = await this.authService.addUser( createUserDto );
        return res.status(HttpStatus.OK).json({
            user: newUser,
        });
    }

    @UseGuards(AuthGuard('local'))
    @Post('/login')
    async login(@Body() body: LoginUserDto) {
        return await this.authService.login(body);
    }
    @UseGuards(AuthGuard('jwt'))
    @Get('/profile')
    getProfile(@Request() req) {
        return req.user;
    }

    @Get('/logout')
    logout(@Request() req) {
        req.logout();
        return 'logged out';
    }

     @UseGuards(AuthGuard('jwt'))
     @Delete(':id')
     deleteUser(@Param('id') id): Promise<any> {
         return this.authService.deleteUser(id);
     }
}
