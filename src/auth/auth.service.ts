import {Model} from 'mongoose';
import { Injectable } from '@nestjs/common';
import { UsersService } from '../users/users.service';
import { JwtService } from '@nestjs/jwt';
import {CreateUserDto} from '../users/dto/create-user.dto';
import {User} from '../users/interfaces/user.interface';
import {InjectModel} from '@nestjs/mongoose';
import * as bcrypt from 'bcrypt';

@Injectable()
export class AuthService {
    constructor(
        @InjectModel('User') private readonly userModel: Model<User>,
        private readonly usersService: UsersService,
        private readonly jwtService: JwtService,
    ) {}

    async  validatePassword(extPassword: string , intPassword: string) {
        return bcrypt.compare(extPassword, intPassword);
    }

    async createHash(password: string) {
        return bcrypt.hash(password, 10);
    }

    async validateUser(username: string, pass: string): Promise<any> {
        const user = await this.usersService.findOne(username);
        if (user && await this.validatePassword( pass, user.password)) {
            const { password, ...result } = user;
            return result;
        }
        return null;
    }
    async login(user: any) {
        const payload = { username: user.username, sub: user.userId };
        return {
            access_token: this.jwtService.sign(payload),
        };
    }

    async addUser(createUserDto: CreateUserDto): Promise< User | string> {
        const existedUser = await this.usersService.findOne(createUserDto.username);
        if (!existedUser) {
            const newUser = await new this.userModel(createUserDto);
            const myPlaintextPassword = newUser.password;
            // @ts-ignore
            newUser.password = await this.createHash(myPlaintextPassword);
            return newUser.save();
        }
        return 'User with this  username is already exist';
    }

    async deleteUser(id: string): Promise<any> {
        return await this.userModel.findByIdAndRemove(id);
    }

}
