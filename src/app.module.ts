import {Module} from '@nestjs/common';
import {AppController} from './app.controller';
import {AppService} from './app.service';
import {MongooseModule} from '@nestjs/mongoose';
import {ContactRequestModule} from './contact-request/contactRequest.module';
import {AuthModule} from './auth/auth.module';
import {UsersModule} from './users/users.module';
import {ConfigModule} from './config/config.module';
import {ConfigService} from './config/config.service';
import { ContactConfigModule } from './contact-config/contact-config.module';
import {ContactConfigSchema} from './contact-config/schemas/contact-config.schema';

const configService = new ConfigService();

@Module({
    imports: [
        ContactConfigModule,
        ContactRequestModule,
        AuthModule,
        UsersModule,
        ConfigModule,
        MongooseModule.forRoot(configService.get('DATABASE_CONNECTION_URL'),
            {useNewUrlParser: true, useUnifiedTopology: true}),
        MongooseModule.forFeature([{ name: 'ContactConfig', schema: ContactConfigSchema }])
    ],
    controllers: [AppController],
    providers: [AppService],
})
export class AppModule {
}
