import { NestFactory } from '@nestjs/core';
import { NestExpressApplication } from '@nestjs/platform-express';
import { AppModule } from './app.module';
import register = require("@react-ssr/nestjs-express/register");


async function bootstrap() {
  const app = await NestFactory.create< NestExpressApplication >(AppModule);
  await register(app);
  app.enableCors();
  await app.listen(3005);
}
bootstrap();
