import * as dotenv from 'dotenv';
import * as fs from 'fs';
dotenv.config();

export class ConfigService {
    private readonly envConfig: Record<string, string>;

    constructor() {
        if (process.env.NODE_ENV === 'local') {
            this.envConfig = dotenv.parse(fs.readFileSync('.env'));
        } else {
            this.envConfig = process.env;
        }
    }

    get(key: string): string {
        return this.envConfig[key];
    }
}
