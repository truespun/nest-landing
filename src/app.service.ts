import {Model} from 'mongoose';
import {Injectable} from '@nestjs/common';
import {InjectModel} from '@nestjs/mongoose';
import {ContactConfig} from './contact-config/interfaces/contact-config.interface';


@Injectable()
export class AppService {
    constructor(@InjectModel('ContactConfig') private readonly contactConfigModel: Model<ContactConfig>  ) {}

    async getContactConfig(): Promise<ContactConfig[]> {
        return await this.contactConfigModel.find().exec();
    }
}
