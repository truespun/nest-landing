module.exports = {
  apps: [
    {
      name: 'landing-backend',
      script: './dist/main.js',
      exec_mode: 'cluster',
      watch: false,
      env: {
        NODE_ENV: 'dev',
        PORT: '3005'
      }
    }
  ]
};
