FROM node:8.16.2-alpine3.9

RUN apk update && apk --no-cache add --virtual builds-deps build-base python

WORKDIR /usr/src/app

COPY package*.json ./

RUN npm install

COPY . .

EXPOSE 3005
CMD [ "npm", "run","start" ]
