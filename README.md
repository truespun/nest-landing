## Change Directory

cd nest-landing

## Copy env (update if needed)

cp .env.example .env


## Installation

npm install


## Running docker 

docker-compose up --build


